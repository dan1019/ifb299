from django.apps import AppConfig


class PinelandsMusicSchoolConfig(AppConfig):
    name = 'PinelandsMusicSchool'
