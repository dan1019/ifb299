from django.urls import path
from . import views

app_name = 'PinelandsMusicSchool'
urlpatterns = [
    path('', views.index, name='index'),
    path('Book_lessons', views.Book_lessons, name= 'Book_lessons'),
    path('Book_lessons/Piano', views.book_piano, name= 'book_piano'),
    path('Hiring_info', views.hiring_info, name= 'hiring_info'),
    path('Hiring_info/Sax', views.hiring_form_sax, name = 'hiring_form_sax'),
    path('Hiring_info/Guitar', views.hiring_form_guitar, name = 'hiring_form_guitar'),
    path('Hiring_info/Flute', views.hiring_form_flute, name = 'hiring_form_flute'),
    path('Hiring_info/Clarinet', views.hiring_form_clarinet, name = 'hiring_form_clarinet'),
    path('Login', views.Login, name='Login'),
    path('Login/create_account', views.create_account, name= 'create_account'),
    path('Student', views.index_student, name= 'index_student'),
    path('Teacher', views.index_Teacher, name= 'index_Teacher'),


]
