from django.http import HttpResponse
from django.shortcuts import render
from django.views import generic



#def index(request):
#    return render(request, 'PinelandsMusicSchool/index.html')


def index(request):
    return render(request, 'PinelandsMusicSchool/index.html')

def Book_lessons(request):
    return render(request, 'PinelandsMusicSchool/lesson_info.html')

def book_piano(request):
    return render(request, 'PinelandsMusicSchool/book_piano.html')

def hiring_info(request):
    return render(request, 'PinelandsMusicSchool/hiring_info.html')

def hiring_form_sax(request):
    return render(request, 'PinelandsMusicSchool/hiring_form_sax.html')

def hiring_form_guitar(request):
    return render(request, 'PinelandsMusicSchool/hiring_form_guitar.html')

def hiring_form_flute(request):
    return render(request, 'PinelandsMusicSchool/hiring_form_flute.html')

def hiring_form_clarinet(request):
    return render(request, 'PinelandsMusicSchool/hiring_form_clarinet.html')

def Login(request):
    return render(request, 'PinelandsMusicSchool/Login.html')

def create_account(request):
    return render(request, 'PinelandsMusicSchool/create_account.html')

def index_student(request):
    return render(request, 'PinelandsMusicSchool/index_student.html')

def index_Teacher(request):
    return render(request, 'PinelandsMusicSchool/index_Teacher.html')
